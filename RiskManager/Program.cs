﻿using System;
using System.ServiceProcess;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;

namespace RiskManager
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetLogger("RiskManager");

        static void Main(string[] args)
        {
            //Always log everything
            NLog.LogManager.GlobalThreshold = NLog.LogLevel.Trace;
            //ServiceStack.Logging.LogManager.LogFactory = new ServiceStack.Logging.NLogger.NLogFactory();

            // In interactive and debug mode ?
            if (Environment.UserInteractive && System.Diagnostics.Debugger.IsAttached)
            {
                // Initialize the service to start
                ServiceBase[] ServicesToRun = new ServiceBase[] { new WinService(new AppHost(), $"http://localhost:{Properties.Settings.Default.Port}/") };

                // Simulate the services execution
                RunInteractiveServices(ServicesToRun);
            }
            else
            {
                // Initialize the service to start
                ServiceBase[] ServicesToRun = new ServiceBase[] { new WinService(new AppHost(), $"{Properties.Settings.Default.Host}:{Properties.Settings.Default.Port.ToString()}/") };

                // Normal service execution
                ServiceBase.Run(ServicesToRun);
            }
        }

        /// <summary>
        /// Run services in interactive mode
        /// </summary>
        static void RunInteractiveServices(ServiceBase[] servicesToRun)
        {
            Console.WriteLine();
            Console.WriteLine("RiskManager Running in Console mode");
            Console.WriteLine();
            logger.Info("RiskManager Running in Console mode");

            // Get the method to invoke on each service to start it
            System.Reflection.MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

            // Start services loop
            foreach (ServiceBase service in servicesToRun)
            {
                Console.Write($"Starting {service.ServiceName} ... ");
                onStartMethod.Invoke(service, new object[] { new string[] { } });
                Console.WriteLine("Started");
                logger.Info($"{service.ServiceName} Started");
            }

            // Waiting the end
            Console.WriteLine();
            Console.WriteLine("Press a key to stop service and terminate...");
            Console.ReadKey();
            Console.WriteLine();

            // Get the method to invoke on each service to stop it
            System.Reflection.MethodInfo onStopMethod = typeof(ServiceBase).GetMethod("OnStop", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

            // Stop loop
            foreach (ServiceBase service in servicesToRun)
            {
                Console.Write($"Stopping {service.ServiceName} ... ");
                onStopMethod.Invoke(service, null);
                Console.WriteLine("Stopped");
                logger.Info($"{service.ServiceName} Stopped");
            }

            Console.WriteLine();
            Console.WriteLine("All services are stopped.");


            // Waiting a key press to not return to VS directly
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine();
                Console.Write("=== Press a key to quit ===");
                Console.ReadKey();
                logger.Info("RiskManager terminated");
                Environment.Exit(0);
            }
        }
    }
}
