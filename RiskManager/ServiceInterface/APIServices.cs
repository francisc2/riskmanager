﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack;
using RiskManager.ServiceModel;
using InstrumentUtils;
using C2Enums;

namespace RiskManager.ServiceInterface
{
    public class APIServices : Service
    {
		private static NLog.Logger logger = NLog.LogManager.GetLogger("APIServices");
        private static RiskController riskController = new RiskController();
		
        /// <summary>
        /// Call this first: it will initialize the RiskController 
        /// </summary>
        /// <param name="hello"></param>
        /// <returns></returns>
        public HelloResponse Any(Hello hello)
        {
			HelloResponse response = new HelloResponse();
			response.Result = $"Hello, {hello.Name}!";
			return response;
        }

        public TakeAvailableSizeResponse Any(TakeAvailableSize request)
        {
            TakeAvailableSizeResponse response = null;
            try
            {
                //Log.LogRequest($"IN (TakeAvailableSize {base.Request?.UserHostAddress})", request?.ToSafeJson());
                Instrument instr = Instrument.ParseC2Symbol(request.FullC2Symbol, RiskController.MDManager.MarketDataCollection.ExchangeHours);
                response = riskController.DoRiskCheck(instr, request);
            }
            catch (Exception ex)
			{
				logger.Error(ex, $"TakeAvailableSize error: " + ex.Message);
				response = new TakeAvailableSizeResponse(RiskResultCode.UnknownError, 0, 0, 0, ex.Message, 0);
				response.ResponseStatus = new ResponseStatus(ex.ToErrorCode(), ex.Message);
			}

            //Log.LogRequest($"OUT (TakeAvailableSize {base.Request?.UserHostAddress})", response?.ToSafeJson());
            return response;
        }

        /// <summary>
        /// Call this first: it will initialize the RiskController 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PrefetchDataStreamsResponse Any(PrefetchDataStreams request)
        {
            PrefetchDataStreamsResponse response = null;
            try
            {
                //Log.LogRequest($"IN (PrefetchDataStreams {base.Request?.UserHostAddress})", request?.ToSafeJson());
                response = riskController.PrefetchDataStreams();
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"PrefetchDataStreams error: " + ex.Message);
                response = new PrefetchDataStreamsResponse() { Result = ex.InnerException.Message };
                response.ResponseStatus = new ResponseStatus(ex.ToErrorCode(), ex.Message);
            }

            //Log.LogRequest($"OUT (PrefetchDataStreams {base.Request?.UserHostAddress})", response?.ToSafeJson());
            return response;
        }
    }
}
