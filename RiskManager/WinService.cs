﻿using System.ServiceProcess;
using ServiceStack;

namespace RiskManager
{
    public partial class WinService : ServiceBase
    {
        private static              NLog.Logger             logger = NLog.LogManager.GetLogger("RiskManager");
        private         readonly    AppHostHttpListenerBase appHost;
        private         readonly    string                  listeningOn;

        public WinService(AppHostHttpListenerBase appHost, string listeningOn)
        {
            //ServiceStack.Logging.LogManager.LogFactory = new ServiceStack.Logging.NLogger.NLogFactory();
            this.appHost = appHost;
            this.listeningOn = listeningOn;

            this.appHost.Init();

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            logger.Info($"Starting RiskManager");
            logger.Info("Log Level is " + NLog.LogManager.GlobalThreshold);
            this.appHost.Start(listeningOn);
            logger.Info("RiskManager started");

            logger.Info("--SETTINGS--");
            logger.Info($"RiskManager listening on {Properties.Settings.Default.Host}: {Properties.Settings.Default.Port}");
            logger.Info($"LogMarketData = {Properties.Settings.Default.LogMarketData}");
            logger.Info("--SETTINGS--");
        }

		protected override void OnStop()
		{
			logger.Info($"stopping RiskManager v{AppHost.Build(System.Reflection.Assembly.GetAssembly(this.GetType()))} on {Properties.Settings.Default.Host}:{Properties.Settings.Default.Port}");
			this.appHost.Stop();
			logger.Info("RiskManager stopped");
		}
    }
}
