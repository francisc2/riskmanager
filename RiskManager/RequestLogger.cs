﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ServiceStack;
using ServiceStack.Web;

namespace RiskManager
{
	class RequestLogger : IRequestLogger
	{
		//Source: https://github.com/ServiceStack/ServiceStack/blob/master/src/ServiceStack/Host/InMemoryRollingRequestLogger.cs

		        internal static long requestId = 0;

        public const int DefaultCapacity = 1000;
        protected readonly ConcurrentQueue<RequestLogEntry> logEntries = new ConcurrentQueue<RequestLogEntry>();
		private static NLog.Logger logger = NLog.LogManager.GetLogger("Requests");


		public RequestLogger()
		{
		}

		/// <summary>
		/// Customize Request Log Entry
		/// </summary>
		public Action<IRequest, RequestLogEntry> RequestLogFilter { get; set; }

		/// <summary>
		/// Turn On/Off Session Tracking
		/// </summary>
		public bool EnableSessionTracking { get; set; }

		/// <summary>
		/// Turn On/Off Raw Request Body Tracking
		/// </summary>
		public bool EnableRequestBodyTracking { get; set; }

		/// <summary>
		/// Turn On/Off Tracking of Responses
		/// </summary>
		public bool EnableResponseTracking { get; set; }

		/// <summary>
		/// Turn On/Off Tracking of Exceptions
		/// </summary>
		public bool EnableErrorTracking { get; set; }

		/// <summary>
		/// Limit logging to only Service Requests
		/// </summary>
		public bool LimitToServiceRequests { get; set; }

		/// <summary>
		/// Limit access to /requestlogs service to role
		/// </summary>
		public string[] RequiredRoles { get; set; }

		/// <summary>
		///  Don't log matching requests
		/// </summary>
		public Func<IRequest, bool> SkipLogging { get; set; }

		/// <summary>
		/// Don't log requests of these types. By default RequestLog's are excluded
		/// </summary>
		public Type[] ExcludeRequestDtoTypes { get; set; }

        /// <summary>
        /// Don't log request bodys for services with sensitive information.
        /// By default Auth and Registration requests are hidden.
        /// </summary>
		public Type[] HideRequestBodyForRequestDtoTypes { get; set; }

		public List<RequestLogEntry> GetLatestLogs(int? take)
		{
			throw new NotSupportedException();
		}

		/// <summary>
		///  Log a request
		/// </summary>
		/// <param name="request">The RequestContext</param>
		/// <param name="requestDto">Request DTO</param>
		/// <param name="response">Response DTO or Exception</param>
		/// <param name="requestDuration">How long did the Request take</param>
        public virtual void Log(IRequest request, object requestDto, object response, TimeSpan requestDuration)
        {
            if (ShouldSkip(request, requestDto))
                return;

            var requestType = requestDto?.GetType();

            var entry = CreateEntry(request, requestDto, response, requestDuration, requestType);

            RequestLogFilter?.Invoke(request, entry);

			if (requestType == null)
				logger.Trace($"{request?.RemoteIp} -> Unkown request type: {entry?.ToSafeJson()}");
			else
				logger.Trace($"{request?.RemoteIp} -> {requestType.Name}: {entry?.ToSafeJson()}");
        }

		public virtual bool ShouldSkip(IRequest req, object requestDto)
		{
			var dto = requestDto ?? req.Dto;
			if (LimitToServiceRequests && dto == null)
				return true;

			var requestType = dto?.GetType();

			return ExcludeRequestType(requestType) || SkipLogging?.Invoke(req) == true;
		}

		/// <summary>
		/// Change what DateTime to use for the current Date (defaults to UtcNow)
		/// </summary>
		 public Func<DateTime> CurrentDateFn { get; set; } = () => DateTime.UtcNow;
        public Func<object, bool> IgnoreFilter { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        protected RequestLogEntry CreateEntry(IRequest request, object requestDto, object response, TimeSpan requestDuration, Type requestType)
		{
			var entry = new RequestLogEntry
			{
				Id = Interlocked.Increment(ref requestId),
				DateTime = CurrentDateFn(),
				RequestDuration = requestDuration,
			};

			if (request != null)
			{
				entry.HttpMethod = request.Verb;
				entry.AbsoluteUri = request.AbsoluteUri;
				entry.PathInfo = request.PathInfo;
				entry.IpAddress = request.UserHostAddress;
				entry.ForwardedFor = request.Headers[HttpHeaders.XForwardedFor];
				entry.Referer = request.Headers[HttpHeaders.Referer];
				entry.Headers = request.Headers.ToDictionary();
				entry.UserAuthId = request.GetHeader(HttpHeaders.XUserAuthId);
				entry.Items = SerializableItems(request.Items);
				entry.Session = EnableSessionTracking ? request.GetSession() : null;
				entry.StatusCode = request.Response.StatusCode;
				entry.StatusDescription = request.Response.StatusDescription;

				var isClosed = request.Response.IsClosed;
				if (false == isClosed)
				{
					entry.UserAuthId = request.GetItemOrCookie(HttpHeaders.XUserAuthId);
					entry.SessionId = request.GetSessionId();
				}

				if (HideRequestBodyForRequestDtoTypes != null
					&& requestType != null
					&& false == HideRequestBodyForRequestDtoTypes.Contains(requestType))
				{
					entry.RequestDto = requestDto;

					if (false == isClosed)
						entry.FormData = request.FormData.ToDictionary();

					if (EnableRequestBodyTracking)
					{
#if NETSTANDARD2_0
                        // https://forums.servicestack.net/t/unexpected-end-of-stream-when-uploading-to-aspnet-core/6478/6
                        if false == request.ContentType.MatchesContentType(MimeTypes.MultiPartFormData))
                        {
                            entry.RequestBody = request.GetRawBody();
                        }
#else
						entry.RequestBody = request.GetRawBody();
#endif
					}
				}
			}

			if (false == response.IsErrorResponse())
			{
				if (EnableResponseTracking)
					entry.ResponseDto = response.GetResponseDto();
			}
			else
			{
				if (EnableErrorTracking)
				{
					entry.ErrorResponse = ToSerializableErrorResponse(response);

					if (response is IHttpError httpError)
					{
						entry.StatusCode = (int)httpError.StatusCode;
						entry.StatusDescription = httpError.StatusDescription;
					}

					if (response is Exception exception)
					{
						if (exception.InnerException != null)
							exception = exception.InnerException;

						entry.ExceptionSource = exception.Source;
						entry.ExceptionData = exception.Data;
					}
				}
			}

			return entry;
		}

		protected bool ExcludeRequestType(Type requestType)
		{
			return ExcludeRequestDtoTypes != null
				   && requestType != null
				   && ExcludeRequestDtoTypes.Contains(requestType);
		}

		public Dictionary<string, string> SerializableItems(Dictionary<string, object> items)
		{
			var to = new Dictionary<string, string>();
			foreach (var item in items)
			{
				var value = item.Value?.ToString() ?? "(null)";
				to[item.Key] = value;
			}

			return to;
		}

		public static object ToSerializableErrorResponse(object response)
		{
			if (response is IHttpResult errorResult)
				return errorResult.Response;

			var ex = response as Exception;
			return ex?.ToResponseStatus();
		}
	}
}