﻿using System;
using Funq;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Globalization;
using ServiceStack;
using ServiceStack.Text;
using RiskManager.ServiceModel;


namespace RiskManager
{
	//VS.NET Template Info: https://servicestack.net/vs-templates/EmptyWindowService
    //See Threading model at https://github.com/ServiceStack/ServiceStack/wiki/Concurrency-model
	public class AppHost : AppHostHttpListenerPoolBase
	{
		static NLog.Logger logger = NLog.LogManager.GetLogger("C2API");
		
		/// <summary>
		/// Base constructor requires a Name and Assembly where web service implementation is located
		/// </summary>
		public AppHost()
			: base("C2API", typeof(RiskManager.ServiceInterface.APIServices).Assembly)
		{
			logger.Info($"Initializing RiskManager v{Build(System.Reflection.Assembly.GetAssembly(this.GetType()))} on {Properties.Settings.Default.Host}:{Properties.Settings.Default.Port}");
		}

		/// <summary>
		/// Application specific configuration
		/// This method should initialize any IoC resources utilized by your web service classes.
		/// </summary>
		public override void Configure(Container container)
		{
			ThreadsPerProcessor = 8;
			JsConfig.TextCase = TextCase.Default;
			JsConfig.AssumeUtc = true;
			JsConfig.AlwaysUseUtc = true;
			JsConfig.ExcludeDefaultValues = true;
			JsConfig.DateHandler = DateHandler.ISO8601;

			SetConfig(new HostConfig
			{//TODO: set 2 and 3 to false and remove Metadata
				EnableFeatures = Feature.All.Remove(Feature.All).Add(Feature.Json).Add(Feature.Metadata),
				DebugMode = false, //Show StackTraces in service responses during development
				WriteErrorsToResponse = false,
				DefaultContentType = MimeTypes.Json,
				AllowSessionIdsInHttpParams = true,
				AllowJsonpRequests = true,
				ApiVersion = "1.0",
				GlobalResponseHeaders = new Dictionary<string, string>
				{
                    { "Vary", "Accept" },
                },
			});

			this.CustomErrorHttpHandlers.Remove(System.Net.HttpStatusCode.Forbidden);

			#region Request Logging

            this.GlobalRequestFilters.Add((request, response, item) =>
            {
                System.Net.IPAddress.TryParse(request?.UserHostAddress, out IPAddress ip);
                logger.Trace($"IN ({request.GetId()}) => {request.PathInfo} | IP={ip?.MapToIPv4()} | {request?.Dto?.ToSafeJson()}");
            });

            this.GlobalResponseFilters.Add((request, response, responseDto) =>
            {
                System.Net.IPAddress.TryParse(request?.UserHostAddress, out IPAddress ip);
                logger.Trace($"OUT ({request.GetId()}) => {request.PathInfo} | IP={ip?.MapToIPv4()} | {response?.Dto?.ToSafeJson()}");
            });

            #endregion

            #region Authentication

            //Only allow localhost

            #endregion
        }

		/// <summary>
		/// Returns the build number for display
		/// </summary>
		/// <returns></returns>
		internal static string Build(System.Reflection.Assembly assembly)
		{
			Version v = assembly.GetName().Version;
			string ret;
			ret = v.Major.ToString(CultureInfo.CurrentCulture) + "." + v.Minor.ToString(CultureInfo.CurrentCulture) + "." + v.Build.ToString(CultureInfo.CurrentCulture);
			return ret;
		}

        /// <summary>
        /// This initializes RiskController and MDManager
        /// </summary>
        public override void OnAfterInit()
        {
            base.OnAfterInit();
            base.ExecuteService(new PrefetchDataStreams()); //Init the service
        }
    }
}