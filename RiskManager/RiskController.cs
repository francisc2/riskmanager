﻿// 
// Copyright (C) 2019 C2 IP LLC
//
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using C2Enums;
using InstrumentUtils;
using QuickFix.Fields;
using System.Runtime.CompilerServices;
using RiskManager.ServiceModel;

namespace RiskManager
{

    /// <summary>
    /// C2Client OS will call DoRiskCheck every 333ms for every RM order.
    /// We take a quote snapshot and keep it for QuoteTTLMS and set the available size to MaxPercentOfBook of topofbook.
    /// We deduce all traded volume from the topofbook quote during those QuoteTTLMS.
    /// When the available size becomes zero, we start holding orders.
    /// After the QuoteTTLMS, we get a fresh quote and reset the available size to MaxPercentOfBook of current topofbook.
    /// 
    /// A-Between 6PM-8:15 AM NY time:
    /// 1-Take MaxPercentOfBook of top of book, then wait QuoteTTLMS.
    /// 2-In cases where MaxPercentOfBook of the book is less than 1 (i.e. when the top-of-book is only 1 unit big), take that 1 unit but then wait 5 seconds.
    /// 3-Each time we do this, increase the wait time by a second, so the next time we take the 1 unit, wait 6 seconds.
    /// 4-Then wait 7 seconds. Stop incrementing at 10 seconds in case it never goes back to be above 1.
    /// 5-This rachet is reset as soon as the top of the book for the particular symbol in question goes back to be above 1.
    /// 
    /// B-Outside those hours:
    /// Same as A except the throttle will be limited to 1 second.
    /// MaxPercentOfBook still applies.
    /// 
    /// C-When CME approves:
    /// Same as A except the throttle will be disabled.
    /// MaxPercentOfBook still applies.
    /// </summary>
    public class RiskController
    {
        private static NLog.Logger logger = NLog.LogManager.GetLogger("RiskManager");
        private const string LogPrefix = "(RiskManager):";
        public static MDManager MDManager;

        /// <summary>
        /// List of static PriceBar objects (not updated with live market data so we control state).
        /// </summary>
        private ConcurrentDictionary<string, DisconnectedQuoteSnapshot> askQuoteSnapshots = new ConcurrentDictionary<string, DisconnectedQuoteSnapshot>();
        private ConcurrentDictionary<string, DisconnectedQuoteSnapshot> bidQuoteSnapshots = new ConcurrentDictionary<string, DisconnectedQuoteSnapshot>();

        private static readonly string BuyStr = QuickFix.Fields.Side.BUY.ToString();


        public RiskController()
        {
            MDManager = new MDManager();
        }


        public TakeAvailableSizeResponse DoRiskCheck(Instrument instr, TakeAvailableSize request)
        {
            try
            {
                #region Lock FullSymbol to ensure only one request per symbol runs at the same time

                DisconnectedQuoteSnapshot quoteSnapshot = null;

                //Start market data if not already started
                MarketData md = MDManager.MarketDataCollection.GetInstrument(instr, true, MDManager.ExchangeHours);

                if (request.Side == BuyStr)
                    askQuoteSnapshots.TryGetValue(instr.C2FullSymbol, out quoteSnapshot);
                else
                    bidQuoteSnapshots.TryGetValue(instr.C2FullSymbol, out quoteSnapshot);

                object lockObject;
                if (quoteSnapshot != null)
                    lockObject = quoteSnapshot;
                else
                    lockObject = new object();

                 #endregion

                lock (lockObject)
                {
                    #region  We take a snapshot of the available volume so we never send more than what's available

                    DisconnectedQuoteSnapshot quote = GetQuoteSnapshot(instr, request.Side, md, quoteSnapshot);
                    if (quote == null)
                    {
                        string logMsg = $"{LogPrefix} Could not get a quote for {instr.C2FullSymbol} so we're sending order as-is";
                        logger.Warn(logMsg);
                        return new TakeAvailableSizeResponse(RiskResultCode.NoQuote, request.OrderQty, 0, 0, logMsg, 0);
                    }

                    #endregion

                    //quote is never NULL after this

                    if (quote.OriginalTopOfBookSize < Properties.Settings.Default.ThrottleIfBookSizeIsLessThan)
                    {
                        long sizeAvailable = quote.RemainingTopOfBookSize;

                        if (sizeAvailable <= 0)
                        {
                            string logMsg = $"{LogPrefix} Not enough remaining {quote.MarketDataType}Size available ({sizeAvailable}) for {instr.C2FullSymbol}: {quote}";
                            logger.Warn(logMsg);
                            return new TakeAvailableSizeResponse(RiskResultCode.NotEnoughSize, 0, quote.RemainingTopOfBookSize, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                        }
                        else if (request.OrderQty == sizeAvailable)
                        {
                            quote.RemainingTopOfBookSize = 0;
                            string logMsg = $"{LogPrefix} OrderQty ({request.OrderQty}) = {quote.MarketDataType}Size ({sizeAvailable}) so we're approving quantity of {sizeAvailable}: {quote}";
                            logger.Debug(logMsg);                         
                            return new TakeAvailableSizeResponse(RiskResultCode.Approved, sizeAvailable, sizeAvailable, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                        }
                        else if (request.OrderQty > sizeAvailable)
                        {
                            quote.RemainingTopOfBookSize = 0;
                            string logMsg = $"{LogPrefix} OrderQty ({request.OrderQty}) > {quote.MarketDataType}Size ({sizeAvailable}) so we're approving quantity of {sizeAvailable}: {quote}";
                            logger.Debug(logMsg);                         
                            return new TakeAvailableSizeResponse(RiskResultCode.PartialApproval, sizeAvailable, sizeAvailable, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                        }
                        else
                        {
                            quote.RemainingTopOfBookSize -= request.OrderQty;
                            string logMsg = $"{LogPrefix} OrderQty ({request.OrderQty}) < {quote.MarketDataType}Size ({quote.RemainingTopOfBookSize}) so we're sending order as-is: {quote}";
                            logger.Debug(logMsg);    
                            return new TakeAvailableSizeResponse(RiskResultCode.Approved, request.OrderQty, sizeAvailable, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                        }
                    }
                    else if (quote.RemainingTopOfBookSize <= 0)
                    {
                        string logMsg = $"{LogPrefix} Not enough remaining {quote.MarketDataType}Size available ({quote.RemainingTopOfBookSize}) for {instr.C2FullSymbol}: {quote}";
                        logger.Warn(logMsg);
                        return new TakeAvailableSizeResponse(RiskResultCode.NotEnoughSize, 0, quote.RemainingTopOfBookSize, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                    }
                    else if (request.OrderQty == quote.RemainingTopOfBookSize)
                    {
                        long sizeAvailable = quote.RemainingTopOfBookSize;
                        quote.RemainingTopOfBookSize = 0;
                        string logMsg = $"{LogPrefix} OrderQty ({request.OrderQty}) = {quote.MarketDataType}Size ({sizeAvailable}) so we're approving quantity of {sizeAvailable}: {quote}";
                        logger.Debug(logMsg);
                        return new TakeAvailableSizeResponse(RiskResultCode.Approved, sizeAvailable, sizeAvailable, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                    }
                    else if (request.OrderQty > quote.RemainingTopOfBookSize)
                    {
                        long sizeAvailable = quote.RemainingTopOfBookSize;
                        quote.RemainingTopOfBookSize = 0;
                        string logMsg = $"{LogPrefix} OrderQty ({request.OrderQty}) > {quote.MarketDataType}Size ({sizeAvailable}) so we're approving quantity of {sizeAvailable}: {quote}";
                        logger.Debug(logMsg);
                        return new TakeAvailableSizeResponse(RiskResultCode.PartialApproval, sizeAvailable, sizeAvailable, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                    }
                    else
                    {
                        long sizeAvailable = quote.RemainingTopOfBookSize;
                        quote.RemainingTopOfBookSize -= request.OrderQty;
                        string logMsg = $"{LogPrefix} OrderQty ({request.OrderQty}) < {quote.MarketDataType}Size ({sizeAvailable}) so we're sending order as-is: {quote}";
                        logger.Debug(logMsg);
                        return new TakeAvailableSizeResponse(RiskResultCode.Approved, request.OrderQty, sizeAvailable, quote.RemainingTopOfBookSize, logMsg, quote.Price);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{LogPrefix} DoRiskCheck error: {ex.Message}");
                return new TakeAvailableSizeResponse(RiskResultCode.UnknownError, 0, 0, 0, ex.Message, 0);
            }
        }

        /// <summary>
        /// Get a curent quote snapshot with size, and add to bidAskSnapshots.
        /// If not available, look in the database.
        /// If no quote, no Price, or no Size, return NULL.
        /// Will always return NULL for non-Futures.        
        /// </summary>
        /// <param name="instr"></param>
        /// <param name="side"></param>
        /// <param name="md"></param>
        /// <param name="newPrice"></param>
        /// <returns></returns>
        public DisconnectedQuoteSnapshot GetQuoteSnapshot(Instrument instr, string side, MarketData md, DisconnectedQuoteSnapshot newPrice)
        {

            if (false == IsSupportedSecurityType(instr.SecurityType) || Instrument.IsExpired(instr.MaturityMonthYear, instr.SecurityType) || md == null)
            {
                return null;
            }
            else
            {
                //Marketdata request will be queued if quote feed is not connected.

                #region Delete existing quote if it expired

                long totalThrottledSoFarMS = 0;
                if (newPrice != null && DateTime.Now >= newPrice.Expires)
                {
                    logger.Debug($"{LogPrefix} Refresh quote for {instr.C2FullSymbol} because it expired at {newPrice.Expires.ToString("HH:mm:ss:fff")}");
                    if (side == BuyStr)
                        _ = askQuoteSnapshots.TryRemove(instr.C2FullSymbol, out _);
                    else
                        _ = bidQuoteSnapshots.TryRemove(instr.C2FullSymbol, out _);

                    //Keep track of it before we set to NULL
                    totalThrottledSoFarMS = newPrice.TotalThrottleMS;

                    //Force a refresh below
                    newPrice = null; 
                }

                #endregion

                if (newPrice == null) //No quote in our cache
                {
                    if (MDManager.ConnectionState != ConnectionState.Connected)
                    {
                        //Market data feed is not connected so don't wait
                        string logMsg = $"{LogPrefix} MarketData feed is not connected. Since we cannot get a {newPrice.MarketDataType} for {instr.C2FullSymbol}, we will send the order as-is";
                        logger.Debug(logMsg);
                        return null;
                    }

                    //Get latest data from our quote provider.
                    newPrice = CreateQuoteSnapShot(md.Bar, side);

                    if (newPrice.Price > 0 && newPrice.OriginalTopOfBookSize > 0)
                    {
                        //We have a valid live quote

                        ThrottleQuote(instr, newPrice, totalThrottledSoFarMS);
                        UpdateSnapshotCollection(instr, newPrice);
                        logger.Debug($"{LogPrefix} New Quote: {newPrice}");
                        return newPrice;
                    }
                    else //no price or no TopOfBookSize
                    {
                        #region Wait MaxWaitForQuoteMs to see if we get a quote. Min is 60ms

                        if (Math.Abs(newPrice.Price) > 0 && newPrice.OriginalTopOfBookSize == 0)
                            logger.Debug($"{LogPrefix} Zero {newPrice.MarketDataType} size available for {instr.C2FullSymbol}. Price= {newPrice.Price}. Waiting {Properties.Settings.Default.MaxWaitForQuoteMs}ms to get another quote");
                        else
                            logger.Debug($"{LogPrefix} Waiting {Properties.Settings.Default.MaxWaitForQuoteMs}ms to get a new {newPrice.MarketDataType} for {instr.C2FullSymbol}");

                        Stopwatch quoteSW = Stopwatch.StartNew();
                        int iterations_30ms = Math.Max(60, Properties.Settings.Default.MaxWaitForQuoteMs) / 30;
                        while (newPrice.Price == 0 && iterations_30ms > 0)
                        {
                            Thread.Sleep(30);
                            iterations_30ms--;
                            newPrice = CreateQuoteSnapShot(md.Bar, side);
                            logger.Debug($"{LogPrefix} waited {quoteSW.ElapsedMilliseconds}ms for quote: {newPrice}");
                        }
                        quoteSW.Stop();

                        #endregion

                        if (newPrice.Price > 0 && newPrice.OriginalTopOfBookSize > 0)
                        {
                            //We have a valid live quote

                            ThrottleQuote(instr, newPrice, totalThrottledSoFarMS);
                            logger.Debug($"{LogPrefix} Getting a {newPrice.MarketDataType} for {instr.C2FullSymbol} took {quoteSW.ElapsedMilliseconds}ms: {newPrice}");
                            UpdateSnapshotCollection(instr, newPrice);
                            return newPrice;
                        }
                        else //Send the order as-is: don't prevent orders from going through if our quote feed doesn't respond within MaxWaitForQuoteMs.
                        {
                            string logMsg = $"{LogPrefix} We waited {quoteSW.ElapsedMilliseconds}ms to get a new {newPrice.MarketDataType} for {instr.C2FullSymbol} but we never received one. Since we cannot get a quote, we will send the order as-is";
                            logger.Debug(logMsg);
                            return null;
                        }
                    }
                }
                else
                {
                    return newPrice;
                }
            }
        }

        /// <summary>
        /// Set DisconnectedQuoteSnapshot TotalThrottleMS and Expires values to throttle the quote for longer periods when TopOfBookSize < ThrottleIfBookSizeIsLessThan
        /// </summary>
        /// <param name="instr"></param>
        /// <param name="newPrice"></param>
        /// <param name="totalThrottledSoFarMS">This is the total quote.TotalThrottleMS throttled so far. Resets when quote expires (default is 333ms)</param>
        private static void ThrottleQuote(Instrument instr, DisconnectedQuoteSnapshot newPrice, long totalThrottledSoFarMS)
        {
            if (false == Properties.Settings.Default.IsThrottleEnabled)
            {
                return;
            }
            else if (newPrice.OriginalTopOfBookSize < Properties.Settings.Default.ThrottleIfBookSizeIsLessThan)
            {
                if (IsNightTimeRegime())
                {
                    //Keep adding to it until we reach 10s
                    if (totalThrottledSoFarMS == 0)
                    {
                        newPrice.TotalThrottleMS = Properties.Settings.Default.InitialThrottleMS;
                        newPrice.Expires = DateTime.Now.AddMilliseconds(newPrice.TotalThrottleMS);
                        newPrice.RemainingTopOfBookSize = Math.Max(newPrice.RemainingTopOfBookSize, 1);
                        logger.Debug($"{LogPrefix} {newPrice.MarketDataType} for {instr.C2FullSymbol} has a TopOfBookSize={newPrice.OriginalTopOfBookSize} so we will keep it for {newPrice.TotalThrottleMS}ms");
                    }
                    else if (totalThrottledSoFarMS < Properties.Settings.Default.MaxThrottleMS)
                    {
                        newPrice.TotalThrottleMS = totalThrottledSoFarMS + Properties.Settings.Default.SubsequentThrottlesMS;
                        newPrice.Expires = DateTime.Now.AddMilliseconds(newPrice.TotalThrottleMS);
                        newPrice.RemainingTopOfBookSize = Math.Max(newPrice.RemainingTopOfBookSize, 1);
                        logger.Debug($"{LogPrefix} {newPrice.MarketDataType} for {instr.C2FullSymbol} has a TopOfBookSize={newPrice.OriginalTopOfBookSize} and it's been throttled {totalThrottledSoFarMS}s so far. We will keep it for {newPrice.TotalThrottleMS}ms");

                    }
                    else if (totalThrottledSoFarMS >= Properties.Settings.Default.MaxThrottleMS)
                    {
                        newPrice.TotalThrottleMS = totalThrottledSoFarMS;
                        newPrice.Expires = DateTime.Now.AddMilliseconds(newPrice.TotalThrottleMS);
                        newPrice.RemainingTopOfBookSize = Math.Max(newPrice.RemainingTopOfBookSize, 1);
                        logger.Debug($"{LogPrefix} {newPrice.MarketDataType} for {instr.C2FullSymbol} has a TopOfBookSize={newPrice.OriginalTopOfBookSize} and it's been throttled {totalThrottledSoFarMS}s so far. MaxThrottle is {Properties.Settings.Default.MaxThrottleMS}ms so we will keep it for {newPrice.TotalThrottleMS}ms");
                    }
                    else //This should never happen
                    {
                        //Reset Throttle
                        logger.Error($"{LogPrefix} Resetting throttle for {newPrice.MarketDataType} {instr.C2FullSymbol}. New TopOfBookSize={newPrice.OriginalTopOfBookSize} and it was throttled {totalThrottledSoFarMS}s. Why did this code trigger?");
                        newPrice.TotalThrottleMS = 0;
                    }
                }
                else
                {
                    //Only throttle 1s
                    newPrice.TotalThrottleMS = 1000;
                    newPrice.Expires = DateTime.Now.AddMilliseconds(newPrice.TotalThrottleMS);
                    newPrice.RemainingTopOfBookSize = Math.Max(newPrice.RemainingTopOfBookSize, 1);
                    logger.Debug($"{LogPrefix} {newPrice.MarketDataType} for {instr.C2FullSymbol} has a TopOfBookSize={newPrice.OriginalTopOfBookSize} so we will keep it for {newPrice.TotalThrottleMS}ms");
                }
            }
            else if (totalThrottledSoFarMS > 0)
            {
                //Reset Throttle
                logger.Debug($"{LogPrefix} Resetting throttle for {newPrice.MarketDataType} {instr.C2FullSymbol}. It has a TopOfBookSize={newPrice.OriginalTopOfBookSize} and it was throttled {totalThrottledSoFarMS}s");
                newPrice.TotalThrottleMS = 0;
            }
        }


        /// <summary>
        /// Create a disconnected object so we do not get marketdata updates
        /// </summary>
        /// <param name="bar"></param>
        /// <param name="fixSide"></param>
        /// <returns></returns>
        private DisconnectedQuoteSnapshot CreateQuoteSnapShot(PriceBar bar, string fixSide)
        {
            if (fixSide == BuyStr)
            {
                return new DisconnectedQuoteSnapshot(bar.Instrument, MarketDataType.Ask, (decimal)bar.Ask.Last, bar.Ask.Timestamp, bar.Ask.TopOfBookSize, Properties.Settings.Default.MaxPercentOfBook, Properties.Settings.Default.QuoteTTLMS);
            }
            else
            {
                return new DisconnectedQuoteSnapshot(bar.Instrument, MarketDataType.Bid, (decimal)bar.Bid.Last, bar.Bid.Timestamp, bar.Bid.TopOfBookSize, Properties.Settings.Default.MaxPercentOfBook, Properties.Settings.Default.QuoteTTLMS);
            }
        }

        private void UpdateSnapshotCollection(Instrument instr, DisconnectedQuoteSnapshot newPrice)
        {
            if (newPrice.MarketDataType == MarketDataType.Ask)
                askQuoteSnapshots.AddOrUpdate(instr.C2FullSymbol, newPrice, (key, oldValue) => newPrice);
            else
                bidQuoteSnapshots.AddOrUpdate(instr.C2FullSymbol, newPrice, (key, oldValue) => newPrice);
        }

        /// <summary>
        /// RiskManager feature will only apply to Futures
        /// </summary>
        /// <param name="fixSecurityType"></param>
        /// <returns></returns>
        public static bool IsSupportedSecurityType(string fixSecurityType)
        {
            return fixSecurityType == QuickFix.Fields.SecurityType.FUTURE;
        }

        /// <summary>
        /// Subscribe to all Futures front months
        /// </summary>
        public PrefetchDataStreamsResponse PrefetchDataStreams()
        {
            try
            {
                logger.Info($"Prefetching subscriptions");
                using (Database.C2.Entities.Context context = new Database.C2.Entities.Context())
                {
                    //About 1800 symbols, all of them front months
                    var allSymbols = context.SymbolsAll.Where(i => i.Frontmonth == "Y" && i.Instrument == "FUTURE").ToList();
                    int cnt = 0;

                    foreach (var item in allSymbols)
                    {
                        Instrument instr = Instrument.ParseC2Symbol(item.Symbol, MDManager.ExchangeHours);

                        //We only have about 52 active futures, so a subset of SymbolsAll
                        IMarket market = MDManager.ExchangeHours.Market(instr.SecurityType, instr.C2RootSymbol);
                        if (market != null)
                        {
                            cnt++;

                            //Start market data
                            MarketData md = MDManager.MarketDataCollection.GetInstrument(instr, true, MDManager.ExchangeHours);
                        }
                    }

                    logger.Info($"Prefetched {cnt} subscriptions");
                    return new PrefetchDataStreamsResponse() { Result = $"prefetched subscriptions to {cnt} symbols" };
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{LogPrefix} PrefetchDataStreams error: {ex.GetBaseException().Message}");
                return new PrefetchDataStreamsResponse() { Result = $"{ex.GetBaseException().Message}", ResponseStatus = new ServiceStack.ResponseStatus("99", $"{ex.Message}") };
            }
        }

        /// <summary>
        /// Between 6PM-8:15 AM NY (i.e. server) time
        /// </summary>
        /// <returns></returns>
        private static bool IsNightTimeRegime()
        {

            if (DateTime.Now.Hour >= 18)
            {
                return true;
            }
            else if (DateTime.Now.Hour < 8)
            {
                return true;
            }
            else if (DateTime.Now.Hour == 8 && DateTime.Now.Minute < 15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
