
//
// This file was generated by the BinaryNotes compiler.
// See http://bnotes.sourceforge.net 
// Any modifications to this file will be lost upon recompilation of the source ASN.1. 
//

using System;
using org.bn.attributes;
using org.bn.attributes.constraints;
using org.bn.coders;
using org.bn.types;
using org.bn;
using RiskManager;

namespace instrumentclass
{


    [ASN1PreparedElement]
    [ASN1Sequence ( Name = "AggregatedQuote", IsSet = false  )]
    public class AggregatedQuote : LogBase, IASN1PreparedElement {
                    
	private string symbol_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "symbol", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string Symbol
        {
            get { return symbol_; }
            set { symbol_ = value;  }
        }
        
                
          
	private string exchangeID_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "exchangeID", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string ExchangeID
        {
            get { return exchangeID_; }
            set { exchangeID_ = value;  }
        }
        
                
          
	private long instrumentType_ ;
	[ASN1Integer( Name = "" )]
    
        [ASN1Element ( Name = "instrumentType", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public long InstrumentType
        {
            get { return instrumentType_; }
            set { instrumentType_ = value;  }
        }
        
                
          
	private string lastTradeDate_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "lastTradeDate", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string LastTradeDate
        {
            get { return lastTradeDate_; }
            set { lastTradeDate_ = value;  }
        }
        
                
          
	private string lastTradeTime_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "lastTradeTime", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string LastTradeTime
        {
            get { return lastTradeTime_; }
            set { lastTradeTime_ = value;  }
        }
        
                
          
	private string lastHigh_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "lastHigh", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string LastHigh
        {
            get { return lastHigh_; }
            set { lastHigh_ = value;  }
        }
        
                
          
	private string lastLow_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "lastLow", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string LastLow
        {
            get { return lastLow_; }
            set { lastLow_ = value;  }
        }
        
                
          
	private string lastClose_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "lastClose", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string LastClose
        {
            get { return lastClose_; }
            set { lastClose_ = value;  }
        }
        
                
          
	private string askTime_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "askTime", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string AskTime
        {
            get { return askTime_; }
            set { askTime_ = value;  }
        }
        
                
          
	private string askHigh_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "askHigh", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string AskHigh
        {
            get { return askHigh_; }
            set { askHigh_ = value;  }
        }
        
                
          
	private string askLow_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "askLow", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string AskLow
        {
            get { return askLow_; }
            set { askLow_ = value;  }
        }
        
                
          
	private string askClose_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "askClose", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string AskClose
        {
            get { return askClose_; }
            set { askClose_ = value;  }
        }
        
                
          
	private string bidTime_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "bidTime", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string BidTime
        {
            get { return bidTime_; }
            set { bidTime_ = value;  }
        }
        
                
          
	private string bidHigh_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "bidHigh", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string BidHigh
        {
            get { return bidHigh_; }
            set { bidHigh_ = value;  }
        }
        
                
          
	private string bidLow_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "bidLow", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string BidLow
        {
            get { return bidLow_; }
            set { bidLow_ = value;  }
        }
        
                
          
	private string bidClose_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "bidClose", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string BidClose
        {
            get { return bidClose_; }
            set { bidClose_ = value;  }
        }
        
                
          
	private long aggregationInterval_ ;
	[ASN1Integer( Name = "" )]
    
        [ASN1Element ( Name = "aggregationInterval", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public long AggregationInterval
        {
            get { return aggregationInterval_; }
            set { aggregationInterval_ = value;  }
        }
        
                
          
	private string c2DataSource_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "c2DataSource", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string C2DataSource
        {
            get { return c2DataSource_; }
            set { c2DataSource_ = value;  }
        }
        
                
          
	private long totalVolume_ ;
	
        private bool  totalVolume_present = false ;
	[ASN1Integer( Name = "" )]
    
        [ASN1Element ( Name = "totalVolume", IsOptional =  true , HasTag =  true, Tag = 0 , HasDefaultValue =  false )  ]
    
        public long TotalVolume
        {
            get { return totalVolume_; }
            set { totalVolume_ = value; totalVolume_present = true;  }
        }
        
                
          
	private long askSize_ ;
	
        private bool  askSize_present = false ;
	[ASN1Integer( Name = "" )]
    
        [ASN1Element ( Name = "askSize", IsOptional =  true , HasTag =  true, Tag = 1 , HasDefaultValue =  false )  ]
    
        public long AskSize
        {
            get { return askSize_; }
            set { askSize_ = value; askSize_present = true;  }
        }
        
                
          
	private long bidSize_ ;
	
        private bool  bidSize_present = false ;
	[ASN1Integer( Name = "" )]
    
        [ASN1Element ( Name = "bidSize", IsOptional =  true , HasTag =  true, Tag = 2 , HasDefaultValue =  false )  ]
    
        public long BidSize
        {
            get { return bidSize_; }
            set { bidSize_ = value; bidSize_present = true;  }
        }
        
                
  
        public bool isTotalVolumePresent () {
            return this.totalVolume_present == true;
        }
        
        public bool isAskSizePresent () {
            return this.askSize_present == true;
        }
        
        public bool isBidSizePresent () {
            return this.bidSize_present == true;
        }
        

            public void initWithDefaults() {
            	
            }


            private static IASN1PreparedElementData preparedData = CoderFactory.getInstance().newPreparedElementData(typeof(AggregatedQuote));
            public IASN1PreparedElementData PreparedData {
            	get { return preparedData; }
            }

            
    }
            
}
