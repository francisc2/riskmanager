
//
// This file was generated by the BinaryNotes compiler.
// See http://bnotes.sourceforge.net 
// Any modifications to this file will be lost upon recompilation of the source ASN.1. 
//

using org.bn.attributes;
using org.bn.coders;
using org.bn;

namespace datacommands
{


    [ASN1PreparedElement]
    [ASN1Sequence ( Name = "GetFundamentalDataCommand", IsSet = false  )]
    public class GetFundamentalDataCommand : IASN1PreparedElement {
                    
	private string exchangeAbbrev_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "exchangeAbbrev", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string ExchangeAbbrev
        {
            get { return exchangeAbbrev_; }
            set { exchangeAbbrev_ = value;  }
        }
        
                
          
	private string instrumentType_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "instrumentType", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string InstrumentType
        {
            get { return instrumentType_; }
            set { instrumentType_ = value;  }
        }
        
                
          
	private string symbol_ ;
	[ASN1String( Name = "", 
        StringType =  UniversalTags.IA5String , IsUCS = false )]
        [ASN1Element ( Name = "symbol", IsOptional =  false , HasTag =  false  , HasDefaultValue =  false )  ]
    
        public string Symbol
        {
            get { return symbol_; }
            set { symbol_ = value;  }
        }
        
                
  

            public void initWithDefaults() {
            	
            }


            private static IASN1PreparedElementData preparedData = CoderFactory.getInstance().newPreparedElementData(typeof(GetFundamentalDataCommand));
            public IASN1PreparedElementData PreparedData {
            	get { return preparedData; }
            }

            
    }
            
}
