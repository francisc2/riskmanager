﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using C2Enums;
using InstrumentUtils;

namespace RiskManager
{
    public class MDManager
    {
        private static	NLog.Logger					    logger							= NLog.LogManager.GetLogger("MDManager");
        private static  RabbitQuoteClient               quoteClient;
        public          ExchangeHours                   ExchangeHours { get; }

        public ConnectionState ConnectionState { get; private set; } = ConnectionState.Disconnected;

        public MDManager()
        {
            //Starts automatically
            logger.Info("MDManager Starting");
            ExchangeHours = new ExchangeHours();
            logger.Info("Waiting for ExchangeHours data to load");
            DateTime startTime = DateTime.UtcNow;
            while (false == ExchangeHours.GotData)
            {
                Thread.Sleep(200);
                if (DateTime.UtcNow.Subtract(startTime).TotalSeconds > 60)
                    logger.Error("Not getting ExchangeHours data from C2. RiskManager will not be fully functional. Will retry in 5 minutes.");
            }

            quoteClient = new RabbitQuoteClient(this);
            MarketDataCollection = new MarketDataCollection(quoteClient, ExchangeHours);
            quoteClient.Connect();
            logger.Info("MDManager Started");
        }

        /// <summary>
        /// Key is the C2 Symbol, not the broker symbol
        /// </summary>
        public MarketDataCollection MarketDataCollection { get; private set; }

        public void OnConnection(ConnectionState priceServer)
        {
            //Use current value if sender does not know the value
            if (priceServer == ConnectionState.Unknown)
                priceServer = ConnectionState;

            //Filter out duplicates
            if (ConnectionState == priceServer)
                return;


            ConnectionState = priceServer;

            logger.Info("Quote feed is " + priceServer);

            if (ConnectionState == ConnectionState.Connected)
                MarketDataCollection.ProcessQueuedSubscriptions();
        }

        internal void ProcessPriceBarEventArgs(PriceBarEventArgs e)
        {
            if (false == e.Process())
                return;

            #region  Save to backup table

            //TODO: (CME) Disabled because it's too much data
            //try
            //{
            //    //Not implemented: only keep one week of data. That's about 1M rows for 10 days of data. Way too much, and every adapter will save teh same data (!).
            //    //Find a way to save 1-min bars and no duplicates
            //    using (Database.Quotes.Entities.Context context = new Database.Quotes.Entities.Context())
            //    {
            //        Database.Quotes.Entities.Bar row = context.Bars.Create();
            //        row.FullC2Symbol = e.MarketData.Instrument.C2FullSymbol;
            //        row.TotalVolume = e.LastBar.TotalVolume; //TODO: (CME) Bar.TotalVolume is for the whole day or just this bar? I also need traded volume for the quote

            //        row.AskClose = (decimal)e.AskBar.Last;
            //        row.AskHigh = (decimal)e.AskBar.High;
            //        row.AskLow = (decimal)e.AskBar.Low;
            //        row.AskOpen = (decimal)e.AskBar.Open;
            //        row.AskSize = e.AskBar.TopOfBookSize;
            //        row.AskTimestamp = e.AskBar.Timestamp;

            //        row.BidClose = (decimal)e.BidBar.Last;
            //        row.BidHigh = (decimal)e.BidBar.High;
            //        row.BidLow = (decimal)e.BidBar.Low;
            //        row.BidOpen = (decimal)e.BidBar.Open;
            //        row.BidSize = e.BidBar.TopOfBookSize;
            //        row.BidTimestamp = e.BidBar.Timestamp;

            //        row.LastClose = (decimal)e.LastBar.Last;
            //        row.LastHigh = (decimal)e.LastBar.High;
            //        row.LastLow = (decimal)e.LastBar.Low;
            //        row.LastOpen = (decimal)e.LastBar.Open;
            //        //row.LastVolume = e.LastBar.TotalVolume;
            //        row.LastTimestamp = e.LastBar.Timestamp;

            //        context.Bars.Add(row);
            //        context.SaveChanges();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    logger.Error(ex, $"Quotes.SaveChangesAsync({e.MarketData.Instrument.C2FullSymbol}) error: {ex.Message}");
            //}

            #endregion
        }
    }
}
