﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    /// <summary>
    /// Overrides ToString() and lists all properties and their values
    /// </summary>
    public abstract class LogBase
    {
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            System.Reflection.BindingFlags flags = System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance;
            System.Reflection.PropertyInfo[] properties = this.GetType().GetProperties(flags);
            int i = 0;
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                //Ignore Lists & collections & empty properties
                var value = property.GetValue(this, null);
                if (value != null)
                {
                    if (i++ == 0)
                        sb.Append(property.Name + "=" + value);
                    else
                        sb.Append(" | " + property.Name + "=" + value);
                }
            }
            return sb.ToString();
        }
    }
}
