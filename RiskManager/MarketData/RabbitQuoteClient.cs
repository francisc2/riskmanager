﻿// 
// Copyright (C) 2013-2021 C2 IP LLC
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using NLog;
using RabbitMQ.Client;
using System.Collections.Concurrent;
using QuickFix.Fields;
using C2Enums;
using InstrumentUtils;
using org.bn;
using RabbitMQ.Client.Events;


namespace RiskManager
{
	class RabbitQuoteClient : IMarketData, IDisposable
	{
		private System.Timers.Timer							connectionTimer = new System.Timers.Timer();
		private MDManager									mdManager;
		private IModel										quotesChannel, statsChannel, commandChannel;
		private IConnection									rabbitConnection;
		private EventingBasicConsumer						quotesConsumer, statsConsumer;
		private bool disposed;
		private IEncoder									encoder = CoderFactory.getInstance().newEncoder();
		private IDecoder									decoder = CoderFactory.getInstance().newDecoder();
		/// <summary>
		/// Internal list of subscribed symbols.  Used on connected event to resubscribe everything.
		/// Key = ConvertSymbolToDtn(securityType, c2RootSymbol, expiry, putOrCall, strikePrice);
		/// </summary>
		private ConcurrentDictionary<string, Instrument>	instrumentList = new ConcurrentDictionary<string, Instrument>();
		private DateTime									lastMsgReceived = DateTime.MinValue;
		private DateTime									lastQuoteReceived = DateTime.MinValue;
		private Dictionary<string, DateTime>				lastAlertSent = new Dictionary<string, DateTime>();
		private RabbitQuoteClientSettings					settings;
		private string										quotesQueueName, statsQueueName, commandQueueName;

		private static Logger logger = LogManager.GetLogger("Rabbit");
		private const int ConnectionMonitorIntervalMs = 16000;
		private const int MaxInactivityDelaySeconds = 15;
		private const string DateFormat = "MM/dd/yyyy";
		private const string TimeFormat = "HH:mm:ss";
		private const string TimeFormatLong = "HH:mm:ss HH:mm:ss";

		/// <summary>
		/// Launch and connect the Rabbit quote feed client
		/// </summary>
		/// <param name="core"></param>
		public RabbitQuoteClient(MDManager core)
		{
			this.settings = new RabbitQuoteClientSettings();
			this.mdManager = core;
			this.quotesQueueName = $"{settings.QueuePrefix}_{settings.ChannelName}_quotes_{ Guid.NewGuid()}";
			this.statsQueueName = $"{settings.QueuePrefix}_{settings.ChannelName}_stats_{ Guid.NewGuid()}";
			this.commandQueueName = $"{settings.QueuePrefix}_{settings.ChannelName}_cmd_{ Guid.NewGuid()}";

			this.connectionTimer.AutoReset = true;
			this.connectionTimer.Interval = ConnectionMonitorIntervalMs;
			this.connectionTimer.Elapsed += new System.Timers.ElapsedEventHandler(connectionTimer_Elapsed);
		}

		#region Connection

		public void Connect()
		{
			if (quotesChannel != null)
			{
				logger.Warn("quotesConnection is already running");
				return;
			}

			//Connection
			ConnectionFactory factory = new ConnectionFactory();
			factory.UserName = settings.User;
			factory.Password = settings.Password;
			factory.VirtualHost = settings.VirtualHost;
			factory.HostName = settings.Host;
			factory.Port = settings.Port;
			factory.RequestedHeartbeat = TimeSpan.FromSeconds(settings.HeartbeatSeconds);
			factory.AutomaticRecoveryEnabled = true;
			// attempt recovery every 10 seconds
			factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);
			rabbitConnection = factory.CreateConnection();

			//Stats
			statsChannel = rabbitConnection.CreateModel();
			statsChannel.QueueDeclare(statsQueueName, false, true, true, null);
			statsChannel.QueueBind(statsQueueName, settings.StatsExchange, settings.StatsBindingKey, null);
			logger.Debug($"Created Stats binding on Channel {statsChannel.ChannelNumber}");

			statsConsumer = new EventingBasicConsumer(statsChannel);
			statsConsumer.Received += (ch, ea) =>
			{
				ProcessStatsMessage(ea);
			};
			String statsConsumerTag = statsChannel.BasicConsume(statsQueueName, true, statsConsumer);
			logger.Debug($"Started Stats consumer {statsConsumerTag}");

			//Commands
			commandChannel = rabbitConnection.CreateModel();
			commandChannel.QueueDeclare(commandQueueName, false, true, true, null);
			//commandChannel.QueueBind(commandQueueName, settings.DataCommandExchange, settings.DataCommandGetQuoteRoutingKey, null);
			logger.Debug($"Created Commands binding");

			//Quotes
			rabbitConnection = factory.CreateConnection();
			quotesChannel = rabbitConnection.CreateModel();
			quotesChannel.QueueDeclare(quotesQueueName, false, true, true, null);
			quotesChannel.QueueBind(quotesQueueName, settings.QuotesStreamExchange, settings.QuotesStreamBindingKey, null);
			logger.Debug($"Created Quotes binding on Channel {quotesChannel.ChannelNumber}");
			quotesConsumer = new EventingBasicConsumer(quotesChannel);
			quotesConsumer.Received += (ch, ea) =>
			{
				ProcessQuoteMessage(ea);
			};
			String quotesConsumerTag = quotesChannel.BasicConsume(quotesQueueName, true, quotesConsumer);
			logger.Debug($"Started Quotes consumer {quotesConsumerTag}");
		}

		/// <summary>
		/// Backup process to detect if the server status
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void connectionTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				if (mdManager.ConnectionState == ConnectionState.Connected
					&& DateTime.Now.Subtract(lastMsgReceived).TotalSeconds > MaxInactivityDelaySeconds
					&& DateTime.Now.Subtract(lastQuoteReceived).TotalSeconds > MaxInactivityDelaySeconds)
				{
					logger.Warn($"CheckConnectionState: More than {MaxInactivityDelaySeconds} seconds without any messages");
					mdManager.OnConnection(ConnectionState.ConnectionLost);
				}
				else if (mdManager.ConnectionState != ConnectionState.Connected
					&& DateTime.Now.Subtract(lastMsgReceived).TotalSeconds < MaxInactivityDelaySeconds
					&& DateTime.Now.Subtract(lastQuoteReceived).TotalSeconds < MaxInactivityDelaySeconds)
				{
					mdManager.OnConnection(ConnectionState.Connected);
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex, "ConnectionTimer Error: " + ex.Message);
			}
		}

		/// <summary>
		/// Returns True during the maintenance period
		/// </summary>
		/// <returns></returns>
		public static bool IsMaintenancePeriod(TimeSpan startTime, TimeSpan endTime)
		{
			DateTime nyTime = DateTime.Now;

			if (nyTime.DayOfWeek == DayOfWeek.Saturday)
				return true;
			else if (nyTime.DayOfWeek == DayOfWeek.Sunday && nyTime.Hour < startTime.Hours)
				return true;
			else if (nyTime.DayOfWeek == DayOfWeek.Friday && (nyTime.Hour >= endTime.Hours || (nyTime.Hour == endTime.Hours && nyTime.Minute >= endTime.Minutes)))
				return true;
			else
				return false;
		}

		private void ResubscribeSymbols()
		{
			KeyValuePair<string, Instrument>[] arr = instrumentList.ToArray();
			instrumentList.Clear();
			logger.Debug($"Resubscribing to {arr.Length} instruments");
			foreach (KeyValuePair<string, Instrument> pair in arr)
				Subscribe(pair.Value);

		}

		#endregion

		#region Process Messages

		/// <summary>
		/// Raised every seconds while IQFeed is connected
		/// </summary>
		/// <param name="e"></param>
		void ProcessStatsMessage(BasicDeliverEventArgs e)
		{
			try
			{
				iqfeedadmindata.IQFeedStatsData msg;
				using (Stream stream = new MemoryStream(e.Body.ToArray()))
				{
					msg = decoder.decode<iqfeedadmindata.IQFeedStatsData>(stream);
				}

				//logger.Trace($"{msg}");
				//logger.Trace($"ProcessStatsMessage IQFeed LoginID={msg.LoginID}|Status={msg.Status}|Symbols={msg.NumberOfSymbols}/{msg.MaxSymbols}|SecondsSinceLastUpdate={msg.SecondsSinceLastUpdate}");

				if (msg.Status != "Connected")
					logger.Debug($"Rabbit DataFeed {msg.LoginID} is {msg.Status}");


				if (String.Compare(msg.Status, "Resubscribe", StringComparison.OrdinalIgnoreCase) > -1)
				{
					logger.Info($"Rabbit DataFeed {msg.LoginID} is {msg.Status}");
					mdManager.OnConnection(ConnectionState.Connected);
					ResubscribeSymbols();
					logger.Debug("Starting ConnectionTimer");
					connectionTimer.Start();
				}
				else if (String.Compare(msg.Status, "Connected", StringComparison.OrdinalIgnoreCase) > -1)
				{
					if (mdManager.ConnectionState != ConnectionState.Connected)
					{
						logger.Info($"Rabbit DataFeed {msg.LoginID} is {msg.Status}");
						mdManager.OnConnection(ConnectionState.Connected);
						ResubscribeSymbols();
						logger.Debug("Starting ConnectionTimer");
						connectionTimer.Start();
					}
					else if (settings.SendConnectionAlarmEmails)
					{
						//Check # subs
						if ((msg.MaxSymbols - msg.NumberOfSymbols) < 75)
						{
							if (false == lastAlertSent.TryGetValue(msg.LoginID, out DateTime lastAlert))
								lastAlertSent.Add(msg.LoginID, lastAlert);

							if (DateTime.UtcNow.Subtract(lastAlert).TotalHours >= 24)
							{
								lastAlertSent[msg.LoginID] = DateTime.UtcNow;
								C2Common.Email.EmailMessage email = new C2Common.Email.EmailMessage(true);
								email.CustomEmail("francis@collective2.com", "IQFeed approaching limit", $"IQFeed {msg.LoginID} has {msg.NumberOfSymbols}/{msg.MaxSymbols} subscriptions");
								email.SaveAsync();
							}
						}

						//Check if active
						if (msg.SecondsSinceLastUpdate > 60)
						{
							C2Common.Email.EmailMessage email = new C2Common.Email.EmailMessage(true);
							email.CustomEmail("francis@collective2.com", "IQFeed appears to be down", $"IQFeed {msg.LoginID} (IP={msg.ServerIP}) is {msg.Status} but it has received no messages in {msg.SecondsSinceLastUpdate}s");
							email.SaveAsync();
						}
					}
				}
				else if (mdManager.ConnectionState == ConnectionState.Connected)
				{
					logger.Warn($"Rabbit DataFeed {msg.LoginID} is {msg.Status}");
					mdManager.OnConnection(ConnectionState.ConnectionLost);
					logger.Debug("Stopping ConnectionTimer");
					connectionTimer.Stop();
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex, "ProcessStatsMessage error: " + ex.Message);
			}
			finally
			{
				lastMsgReceived = DateTime.Now;
			}
		}

		void ProcessQuoteMessage(BasicDeliverEventArgs e)
		{
			instrumentclass.AggregatedQuote quote = null;

			try
			{
				using (Stream stream = new MemoryStream(e.Body.ToArray()))
				{
					quote = decoder.decode<instrumentclass.AggregatedQuote>(stream);
				}

				//LastTradeDate&Time is never empty if any price exists in bid/ask/last (but Last price may be zero!)
				//Last refers to the Last Trade which may not be available (e.g. after a IQFeed restart).
				//Ignore the a,b,t in the LastTradeTime field because it's meaningless for AggregatedQuotes. It's just the last value returned by IQFeed but there could have been many updates in the AggregatedQuote
				//String metaData = String.Format("{0} {1} {2} {3}", quote.Symbol, quote.InstrumentType, quote.ExchangeID, quote.AggregationInterval);

				//String info = String.Format(@"Bid: {0}  {1}  {2}  {3} Ask: {4}  {5}  {6}  {7} Trade: {8}  {9}  {10}  {11}  {12}",
				//              quote.BidTime, quote.BidLow,
				//              quote.BidHigh, quote.BidClose,
				//              quote.AskTime, quote.AskLow,
				//              quote.AskHigh, quote.AskClose,
				//              quote.LastTradeDate, quote.LastTradeTime,
				//              quote.LastLow, quote.LastHigh, quote.LastClose);
				//string result = metaData + " " + info;
				//@ESU13 8 us 500 Bid:   0.0  0.0  0.0 Ask: 05:39:52  1658.25  1658.25  1658.25 Trade: 08/16/2013  05:39:41a  0.0  0.0  0.0
				//@ESU13 8 us 500 Bid: 05:39:53  1658.00  1658.00  1658.00 Ask: 05:39:53  1658.25  1658.25  1658.25 Trade: 08/16/2013  05:39:41a  0.0  0.0  0.0
				//@ESU13 8 us 500 Bid: 05:39:54  1658.00  1658.00  1658.00 Ask: 05:39:53  1658.25  1658.25  1658.25 Trade: 08/16/2013  05:39:41b  0.0  0.0  0.0

				//Filter out what we don't subscribe to or junk data
				if (quote.AggregationInterval < 1 || false == instrumentList.ContainsKey(quote.Symbol))
					return;

				Instrument instrument = instrumentList[quote.Symbol];
				MarketData marketData = mdManager.MarketDataCollection.GetInstrument(instrument, false, mdManager.ExchangeHours);
				Bar askBar = GetQuoteData(marketData.Bar.Ask, "Ask", quote.LastTradeDate, quote.AskTime, quote.AskHigh, quote.AskLow, quote.AskClose, quote.TotalVolume, quote.AskSize);
				Bar bidBar = GetQuoteData(marketData.Bar.Bid, "Bid", quote.LastTradeDate, quote.BidTime, quote.BidHigh, quote.BidLow, quote.BidClose, quote.TotalVolume, quote.BidSize);

				//Watch out for the b/a/t value at the end of LastTradeTime
				Bar lastBar = GetQuoteData(marketData.Bar.Last, "Last", quote.LastTradeDate, quote.LastTradeTime.Substring(0, quote.LastTradeTime.Length - 1), quote.LastHigh, quote.LastLow, quote.LastClose, quote.TotalVolume, 0);

				//Call synchronously so price bars update while we process a new signal
				mdManager.ProcessPriceBarEventArgs(new PriceBarEventArgs(marketData, bidBar, askBar, lastBar, mdManager.ConnectionState, DateTime.UtcNow, C2Common.Globals.LogMarketData));
			}
			catch (Exception ex)
			{
				logger.Error(ex, $"ProcessQuoteMessage error: {ex.Message}. {quote?.ToString()}");
			}
			finally
			{
				lastQuoteReceived = DateTime.Now;
			}
		}

		/// <summary>
		/// Returns a Bar object with a symbol and valid prices. Zero values will be returned if no valid price is received or if a parsing error occurs.
		/// Bar.Open will always be == oldBar.Close
		/// Bar.Timestamp will be DateTime.MinValue if no valid date is provided
		/// </summary>
		/// <param name="oldBar"></param>
		/// <param name="quoteType"></param>
		/// <param name="dateString"></param>
		/// <param name="timeString"></param>
		/// <param name="highString"></param>
		/// <param name="lowString"></param>
		/// <param name="lastString"></param>
		/// <returns></returns>
		private Bar GetQuoteData(Bar oldBar, string quoteType, string dateString, string timeString, string highString, string lowString, string lastString, long totalVolume, long size)
		{
			Bar newbar = new Bar(oldBar);

			#region Dates

			//DateTimes arrive in NY time zone
			DateTime quoteDate, quoteTime;
			if (dateString.Length == 0)
			{
				if (lastString != "0.0" && lastString != "")
					logger.Debug($"Missing LastTradeDate in AggregatedQuote for {oldBar.RootSymbol}. Ignoring quote");
				return newbar;
			}
			else if (timeString.Length == 0)
			{
				if (lastString != "0.0" && lastString != "")
					logger.Debug($"Missing {quoteType}Time in AggregatedQuote for {oldBar.RootSymbol}. Ignoring quote");
				return newbar;
			}
			else if (!DateTime.TryParseExact(dateString, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out quoteDate))
			{
				if (lastString != "0.0" && lastString != "")
					logger.Warn($"Invalid AggregatedQuote {quoteType}Date for {oldBar.RootSymbol}: '{dateString}'. Ignoring quote");
				return newbar;
			}
			else if (!DateTime.TryParseExact(timeString, TimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out quoteTime))
			{
				if (lastString != "0.0" && timeString != "99:99:99")
					logger.Warn($"Invalid AggregatedQuote {quoteType}Time for {oldBar.RootSymbol}: '{timeString}'. Ignoring quote");
				return newbar;
			}

			//Save in NY time zone
			newbar.Timestamp = new DateTime(quoteDate.Year, quoteDate.Month, quoteDate.Day, quoteTime.Hour, quoteTime.Minute, quoteTime.Second);

			#endregion

			#region Prices

			double high, low, last;

			if (!Double.TryParse(highString, NumberStyles.Float, CultureInfo.InvariantCulture, out high) && highString != "")
				logger.Warn($"Invalid AggregatedQuote {quoteType}High price for {oldBar.RootSymbol}: {highString}");

			if (!Double.TryParse(lowString, NumberStyles.Float, CultureInfo.InvariantCulture, out low) && lowString != "")
				logger.Warn($"Invalid AggregatedQuote {quoteType}Low price for {oldBar.RootSymbol}: {lowString}");

			if (!Double.TryParse(lastString, NumberStyles.Float, CultureInfo.InvariantCulture, out last) && lastString != "")
				logger.Warn($"Invalid AggregatedQuote {quoteType}Close price for {oldBar.RootSymbol}: {lastString}");

			if (!MarketData.IsPriceValid(high))
				high = 0;
			if (!MarketData.IsPriceValid(low))
				low = 0;

			if (!MarketData.IsPriceValid(last))
				last = 0;
			else
				newbar.Open = oldBar.Last;

			//Keep data format in line with C2. DTN changed the format and it's too complicated to change historical C2 data, not to mention vendor disruption.
			if (oldBar.RootSymbol == "@CT" || oldBar.RootSymbol == "QHG" || oldBar.RootSymbol == "@QC")
			{
				high *= 100;
				low *= 100;
				last *= 100;
			}

			if (last > 0)
				newbar.Last = last;
			if (high > 0)
				newbar.High = high;
			if (low > 0)
				newbar.Low = low;

			#endregion

			#region Volume

			newbar.TopOfBookSize = size;
			newbar.TotalVolume = totalVolume;

			#endregion

			return newbar;
		}

		///// <summary>
		///// We will get bid/ask/last only for IQFeed.
		///// For CQG, we only get Last price.
		///// </summary>
		///// <param name="quote"></param>
		//private void quotesDataFeed_OnQuote(instrumentclass.InstrumentStr quote)
		//{
		//          try
		//          {
		//              if (!instrumentList.ContainsKey(quote.Symbol))
		//                  return;

		//              DateTime lastDate, lastTime, bidTime, askTime;
		//              Instrument instrument = instrumentList[quote.Symbol];
		//              MarketData marketData = core.MarketDataCollection.GetInstrument(instrument, false);

		//              //Last Price
		//              if (DateTime.TryParseExact(quote.LastTradeTime.Substring(0, 8), TimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out lastTime)
		//                  && DateTime.TryParseExact(quote.LastTradeDate.Substring(0, 10), DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out lastDate))
		//              {
		//                  double lastPrice;
		//                  DateTime tradeDateTime = new DateTime(lastDate.Year, lastDate.Month, lastDate.Day, lastTime.Hour, lastTime.Minute, lastTime.Second);
		//                  if (Double.TryParse(quote.Last, NumberStyles.Float, CultureInfo.InvariantCulture, out lastPrice))
		//                  {
		//                      //Keep data format in line with C2. DTN changed the format and it's too complicated to change historical C2 data, not to mention vendor disruption.
		//                      if (quote.Symbol.StartsWith("@CT") || quote.Symbol.StartsWith("QHG") || quote.Symbol.StartsWith("@QC"))
		//                          lastPrice *= 100;

		//                      if (marketData.Quote.Last != lastPrice || marketData.Quote.LastTime != tradeDateTime)
		//                      {
		//                          core.ProcessEventArgs(new MarketDataEventArgs(marketData, MarketDataType.Last, lastPrice, core.PriceConnectionState, tradeDateTime.ToUniversalTime()));

		//                          //We will not get bid'ask on cqg so simulate it
		//                          if (quote.C2DataSource == "cqg")
		//                          {
		//                              core.ProcessEventArgs(new MarketDataEventArgs(marketData, MarketDataType.Bid, lastPrice, core.PriceConnectionState, tradeDateTime.ToUniversalTime()));
		//                              core.ProcessEventArgs(new MarketDataEventArgs(marketData, MarketDataType.Ask, lastPrice, core.PriceConnectionState, tradeDateTime.ToUniversalTime()));
		//                          }
		//                      }
		//                  }
		//                  else
		//                  {
		//                      logger.Warn($"Invalid IQFeed Last price for {quote.Symbol}: {quote.Last}");
		//                  }
		//              }

		//              //Bid
		//              if (quote.BidTime.Length > 0 && quote.Bid.Length > 0)
		//              {
		//                  if (DateTime.TryParseExact(quote.BidTime.Substring(0, 8), TimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out bidTime))
		//                  {
		//                      double bidPrice;
		//                      if (Double.TryParse(quote.Bid, NumberStyles.Float, CultureInfo.InvariantCulture, out bidPrice))
		//                      {
		//                          //Keep data format in line with C2. DTN changed the format and it's too complicated to change historical C2 data, not to mention vendor disruption.
		//                          if (quote.Symbol.StartsWith("@CT") || quote.Symbol.StartsWith("QHG") || quote.Symbol.StartsWith("@QC"))
		//                              bidPrice *= 100;

		//                          if (marketData.Quote.Bid != bidPrice || marketData.Quote.BidTime != bidTime)
		//                              core.ProcessEventArgs(new MarketDataEventArgs(marketData, MarketDataType.Bid, bidPrice, core.PriceConnectionState, bidTime.ToUniversalTime()));
		//                      }
		//                      else
		//                      {
		//                          logger.Warn($"Invalid IQFeed Bid price for {quote.Symbol}: {quote.Bid}");
		//                      }
		//                  }
		//              }

		//              //Ask
		//              if (quote.AskTime.Length > 0 && quote.Ask.Length > 0)
		//              {
		//                  if (DateTime.TryParseExact(quote.AskTime.Substring(0, 8), TimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out askTime))
		//                  {
		//                      double askPrice;
		//                      if (Double.TryParse(quote.Ask, NumberStyles.Float, CultureInfo.InvariantCulture, out askPrice))
		//                      {
		//                          //Keep data format in line with C2. DTN changed the format and it's too complicated to change historical C2 data, not to mention vendor disruption.
		//                          if (quote.Symbol.StartsWith("@CT") || quote.Symbol.StartsWith("QHG") || quote.Symbol.StartsWith("@QC"))
		//                              askPrice *= 100;

		//                          if (marketData.Quote.Ask != askPrice || marketData.Quote.AskTime != askTime)
		//                              core.ProcessEventArgs(new MarketDataEventArgs(marketData, MarketDataType.Ask, askPrice, core.PriceConnectionState, askTime.ToUniversalTime()));
		//                      }
		//                      else
		//                      {
		//                          logger.Warn($"Invalid IQFeed Ask price for {quote.Symbol}: {quote.Ask}");
		//                      }
		//                  }
		//              }
		//          }
		//          catch (Exception ex)
		//          {
		//              string msg = $"Symbol={quote.Symbol}|LastTradeTime={quote.LastTradeTime}|LastTradeDate={quote.LastTradeDate}|BidTime={quote.BidTime}|AskTime={quote.AskTime}|BidSize={quote.BidSize}|AskSize={quote.AskSize}|";
		//              logger.Error(ex, "OnQuote.Process error: " + msg + ex.Message);
		//          }
		//          finally
		//          {
		//              lastQuoteReceived = DateTime.Now;
		//          }
		//}

		#endregion

		#region IMarketData Members

		public void Subscribe(Instrument instrument)
		{
			try
			{
				///I cannot be sure the caller will always be the same thread, so let's make sure we don't have concurrency issues with rabbitmq
				string c2SecurityType = GetC2SecurityType(instrument.SecurityType);
				string market = GetMarketCode(instrument, mdManager.MarketDataCollection.ExchangeHours);
				string group = GetGroup(instrument);
				string dataSourceId = GetDataSourceId(instrument);
				string fullSymbol;
				if (dataSourceId == "cqg")
					fullSymbol = SymbolConversion.ConvertSymbolToCqg(instrument);
				else
					fullSymbol = SymbolConversion.ConvertSymbolToDtn(instrument.SecurityType, instrument.C2RootSymbol, instrument.SecurityExchange, instrument.MaturityMonthYear, instrument.UnderlyingMaturityMonthYear, instrument.PutOrCall, instrument.StrikePrice);

				if (false == instrumentList.ContainsKey(fullSymbol))
				{
					if (false == quotesChannel.IsOpen)
					{
						logger.Debug($"Rabbit Quote Feed is not connected, adding message to buffer: Subscribe to {Instrument.ToHumanSymbol(instrument)}");
					}
					else
					{
						string routingKey = CreateRoutingKey(fullSymbol, c2SecurityType, dataSourceId, market, group);
						string bindingKey = CreateBindingKey(fullSymbol);
						logger.Debug($"BasicPublish({routingKey}) and QueueBind({bindingKey})");

						lock (commandChannel)
						{
							datacommands.GetQuoteCommand cmd = new datacommands.GetQuoteCommand();
							cmd.ExchangeAbbrev = market;
							cmd.InstrumentType = c2SecurityType;
							cmd.Symbol = fullSymbol;
							using (MemoryStream stream = new MemoryStream())
							{
								encoder.encode<datacommands.GetQuoteCommand>(cmd, stream);
								commandChannel.BasicPublish(settings.DataCommandExchange, routingKey, null, stream.ToArray());
							}
						}

						quotesChannel.QueueBind(quotesQueueName, settings.QuotesStreamExchange, bindingKey);
					}

					instrumentList.TryAdd(fullSymbol, instrument);
				}
				else
				{
					logger.Debug($"Already subscribed to Rabbit Quote Feed for {fullSymbol} ({Instrument.ToHumanSymbol(instrument)})");
				}
			}
			catch (TimeoutException ex)
			{
				logger.Warn($"RabbitQuoteClient.Subscribe failed for {Instrument.ToHumanSymbol(instrument)}: {ex.Message}. Trying again a bit later");
			}
			catch (Exception ex)
			{
				logger.Error(ex, $"RabbitQuoteClient.Subscribe failed for {Instrument.ToHumanSymbol(instrument)}: {ex.Message}");
			}
		}

		public void Unsubscribe(Instrument instrument)
		{
			try
			{
				string dataSourceId = GetDataSourceId(instrument);
				string c2SecurityType = GetC2SecurityType(instrument.SecurityType);
				string market = GetMarketCode(instrument, mdManager.MarketDataCollection.ExchangeHours);
				string group = GetGroup(instrument);
				string fullSymbol;
				if (dataSourceId == "cqg")
					fullSymbol = SymbolConversion.ConvertSymbolToCqg(instrument);
				else
					fullSymbol = SymbolConversion.ConvertSymbolToDtn(instrument.SecurityType, instrument.C2RootSymbol, instrument.SecurityExchange, instrument.MaturityMonthYear, instrument.UnderlyingMaturityMonthYear, instrument.PutOrCall, instrument.StrikePrice);

				if (instrumentList.TryRemove(fullSymbol, out _))
				{
					if (quotesChannel.IsOpen)
					{
						string bindingKey = CreateBindingKey(fullSymbol);
						logger.Debug($"Sending QueueUnbind({bindingKey})");

						//commandChannel.BasicPublish(settings.DataCommandExchange, settings.DataCommandGetQuoteRoutingKey, null, Encoding.UTF8.GetBytes(routingKey));
						quotesChannel.QueueUnbind(quotesQueueName, settings.QuotesStreamExchange, bindingKey);
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex, $"RabbitQuoteClient.Unsubscribe failed for {Instrument.ToHumanSymbol(instrument) }: {ex.Message}");
			}
		}

		public ConnectionState PriceConnectionState
		{
			get { return mdManager.ConnectionState; }
		}

		#endregion

		#region Converters

		string CreateRoutingKey(String symbol, String instrument, String dataSourceId, String market, String group)
		{
			//US stocks: iq.qcmd.US.stock.CCI.AN and quote.CCI
			//Forex: iq.qcmd.US.forex.EURUSD^]FXCM.1 and quote.EURUSD^]FXCM
			//Options: iq.qcmd.US.option.SPY1926H287^]5.1
			//London: "iq.qcmd.LSE.stock.L^]IAG.1" and "quote.L^]IAG"
			//Toronto: iq.qcmd.TSX.stock.C^]BPF.TSX and "quote.C^]BPF"
			return String.Format("{0}.{1}.{2}.{3}.{4}.{5}",
				dataSourceId,
				settings.DataCommandGetQuoteRoutingKey,
				EncodeKey(market),
				EncodeKey(instrument),
				EncodeKey(symbol),
				EncodeKey(group)
				);
		}

		static string CreateBindingKey(string fullSymbol)
		{
			return $"quote.{EncodeKey(fullSymbol)}";
		}

		// <summary>
		/// =============================================================================
		/// @doc Converts AMQP 'semantic' chars to other.
		///
		/// AMQP cannot use '.' and '*' inside akey. I think also '#' is danger.
		/// So we need encode keys somehow.
		///
		/// This function performs such encoding. A result is a binary.
		///
		/// '.' -> '|'
		/// '*' -> '{'
		/// '#' -> '}'
		/// 
		/// CHANGED 2012-03-13: Convert dot only.
		/// '.' -> '^]'  (= Group separator http://en.wikipedia.org/wiki/Group_separator#Field_separators)
		/// 
		/// @end
		/// =============================================================================
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		private static string EncodeKey(string key)
		{
			//string result = key.Replace('.', '|');
			//result = result.Replace('*', '{');
			//result = result.Replace('#', '}');
			//return result;
			return key.Replace(".", "^]");
		}

		private static string DecodeKey(string key)
		{
			//string result = key.Replace('|', '.');
			//result = result.Replace('{', '*');
			//result = result.Replace('}', '#');
			//return result;
			return key.Replace("^]", ".");
		}

		private static string GetC2SecurityType(string fixSecurityType)
		{
			if (fixSecurityType == SecurityType.COMMON_STOCK)
				return "stock";
			else if (fixSecurityType == SecurityType.FUTURE)
				return "future";
			else if (fixSecurityType == SecurityType.OPTION || fixSecurityType == SecurityType.OPTIONS_ON_FUTURES)
				return "option";
			else if (fixSecurityType == SecurityType.FOREIGN_EXCHANGE_CONTRACT)
				return "forex";
			else
				return "unknown";
		}

		/// <summary>
		/// Allows splitting the requests into groups so we can setup multiple IQfeed connections if necessary
		/// Group "AN" -> first IQFeed machine
		/// Group "OZ" -> 2nd IQFeed machine
		/// </summary>
		/// <param name="instrument"></param>
		/// <returns></returns>
		private static string GetGroup(Instrument instrument)
		{
			if (instrument.SecurityType == SecurityType.COMMON_STOCK)
			{
				if (instrument.SecurityExchange == Exchange.Tse)
				{
					return "TSX";
				}
				else if (instrument.SecurityExchange != Exchange.Default)
				{
					//All non-US stocks must go to CQG
					//2021-01: we only support London and Toronto 
					return "1";
				}
				else if (instrument.C2RootSymbol[0] > 'N')
				{
					return "OZ";
				}
				else
				{
					return "AN";
				}
			}
			else
			{
				return "1";
			}
		}

		/// <summary>
		/// returns the C2 exchange code from exchangehours table
		/// </summary>
		/// <param name="instrument"></param>
		/// <returns></returns>
		private static string GetMarketCode(Instrument instrument, ExchangeHours hours)
		{
			C2Enums.IMarket market = hours.Market(instrument.SecurityType, instrument.C2RootSymbol);
			if (market != null)
			{
				return market.C2Exchange;
			}
			else
			{
				return GetDefaultC2Exchange(instrument);
			}
		}

		/// <summary>
		/// "US" for US symbols.
		/// For non-US exchanges symbols use CQG echanges.
		/// </summary>
		/// <param name="instrument"></param>
		/// <returns></returns>
		private static string GetDefaultC2Exchange(Instrument instrument)
		{
			switch (instrument.SecurityExchange)
			{
				case C2Enums.Exchange.Cme:
					return "CME";
				case C2Enums.Exchange.Nybot:
					return "NYB";
				case C2Enums.Exchange.Cbot:
					return "CBT";
				case C2Enums.Exchange.Eurex:
					return "EUREX";
				case C2Enums.Exchange.Comex:
					return "CMX";
				case C2Enums.Exchange.Nymex:
					return "NYM";
				case C2Enums.Exchange.Kcbt:
					return "KCB";
				case C2Enums.Exchange.Sgx:
					return "SIMEX";
				case C2Enums.Exchange.Ice:
					return "ICE";
				case C2Enums.Exchange.Mse:
					return "MX";
				case C2Enums.Exchange.Sfe:
					return "SFE";
				case C2Enums.Exchange.Xetra:
					return "XETRA";
				case C2Enums.Exchange.Asx:
					return "ASX";
				case C2Enums.Exchange.Mef:
					return "MEFF";
				case C2Enums.Exchange.Hkfe:
					return "HSFE";
				case C2Enums.Exchange.Nse:
					return "NSI";
				case C2Enums.Exchange.Lse:
					return "LSE";
				case C2Enums.Exchange.Tse:
					return "TSX";
				default:
					return "US";
			}
		}

		/// <summary>
		/// - "iq" for all symbols,
		/// </summary>
		/// <param name="instrument"></param>
		/// <returns></returns>
		private static string GetDataSourceId(Instrument instrument)
		{
			return "iq";
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.disposed)
			{
				// If disposing equals true, dispose all managed 
				// and unmanaged resources.
				if (disposing)
				{
					// Dispose managed resources.
					if (connectionTimer != null)
						connectionTimer.Dispose();

					if (quotesChannel != null && quotesChannel.IsOpen)
						quotesChannel.Close();

					//if (commandChannel != null && commandChannel.IsOpen)
					//	commandChannel.Close();

					if (statsChannel != null && statsChannel.IsOpen)
						statsChannel.Close();

					if (rabbitConnection != null && rabbitConnection.IsOpen)
						rabbitConnection.Close();

					logger.Debug("RabbitQuoteClient stopped");
				}
			}
			disposed = true;
		}

		#endregion
	}
}
