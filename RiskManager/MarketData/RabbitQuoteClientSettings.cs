﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using C2Enums;
using System.Threading.Tasks;
using NLog;

namespace RiskManager
{
	public sealed class RabbitQuoteClientSettings : LogBase
	{
		private readonly string filePathName = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\RabbitQuoteClientSettings.xml";

		private static Logger logger = LogManager.GetLogger("RabbitQuoteClientSettings");
		public bool AggregatedQuotesEnabled { get; }
		public string Host { get; }
		public int Port { get; }

		public string ChannelName { get; }
		public bool SendConnectionAlarmEmails { get; } = false;

		public int HeartbeatSeconds { get; }
		public string VirtualHost { get; }
		public string User { get; }
		public string Password { get; }
		public string QueuePrefix { get; }
		public string DataCommandExchange { get; }
		public string DataCommandExchangeType { get; }
		public string DataCommandGetQuoteRoutingKey { get; }
		public string QuotesStreamExchange { get; }
		public string QuotesStreamExchangeType { get; }
		public string QuotesStreamBindingKey { get; }
		public string StatsExchange { get; }
		//public string StatsExchangeType { get; } = GetSetting("StatsExchangeType");
		public string StatsBindingKey { get; }

		private const string nodeKey = "RabbitQuoteClient";

		public RabbitQuoteClientSettings()
		{
			// Create settings file
			if (false == File.Exists(filePathName))
			{
				try
				{
					XmlDocument x = new XmlDocument();

					//Create headers
					x.LoadXml("<Settings></Settings>");
					x.InsertBefore(x.CreateXmlDeclaration("1.0", "utf-8", null), x.DocumentElement);
					x.Save(filePathName);
				}
				catch (XmlException ex)
				{
					logger.Error(ex, "Could not create " + filePathName + ": " + ex.Message + "  Will use default RabbitQuoteClientSettings values");
				}
			}

			AggregatedQuotesEnabled = bool.Parse(GetSetting("AggregatedQuotesEnabled"));
			Host = GetSetting("Host");
			Port = Int32.Parse(GetSetting("Port"), CultureInfo.InvariantCulture);
			ChannelName = GetSetting("ChannelName");
			SendConnectionAlarmEmails = bool.Parse(GetSetting("SendConnectionAlarmEmails"));
			HeartbeatSeconds = Int32.Parse(GetSetting("HeartbeatSeconds"), CultureInfo.InvariantCulture);
			VirtualHost = GetSetting("VirtualHost");
			User = GetSetting("User");
			Password = GetSetting("Password");
			QueuePrefix = GetSetting("QueuePrefix");
			DataCommandExchange = GetSetting("DataCommandExchange");
			DataCommandGetQuoteRoutingKey = GetSetting("DataCommandGetQuoteRoutingKey");
			QuotesStreamExchange = GetSetting("QuotesStreamExchange");
			QuotesStreamBindingKey = GetSetting("QuotesStreamBindingKey"); // It includes all 3 types of quotes: Trade, Ask, Bid
			StatsExchange = GetSetting("StatsExchange");
			StatsBindingKey = GetSetting("StatsBindingKey");
			DataCommandExchangeType = GetSetting("DataCommandExchangeType");
			QuotesStreamExchangeType = GetSetting("QuotesStreamExchangeType");
		}

		public string GetSetting(string key)
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				using (XmlTextReader reader = new XmlTextReader(filePathName))
				{
					doc.Load(reader);
					reader.Close();
				}

				//Create elements if not already there
				if (doc["Settings"][nodeKey] == null)
				{
					doc["Settings"].AppendChild(doc.CreateElement(nodeKey));
					doc.Save(filePathName);
				}
				if (doc["Settings"][nodeKey][key] == null)
				{
					logger.Debug($"The key <Settings><{nodeKey}><{key}> does not exist in {filePathName}.  Using Default value.");
					SaveSetting(key, Default(key));
					return Default(key);
				}

				return doc["Settings"][nodeKey][key].InnerText;
			}
			catch (XmlException ex)
			{
				logger.Error(ex, $"Cannot read <Settings><{nodeKey}><{key}> from {filePathName}: {ex.Message}");
				return Default(key);
			}
		}

		/// <summary>
		/// Values are formatted as Invariant Culture strings before they get here.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <param name="encrypt"></param>
		private void SaveSetting(string key, string value)
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				using (XmlTextReader reader = new XmlTextReader(filePathName))
				{
					doc.Load(reader);
					reader.Close();
				}

				//Create element if not already there
				if (doc["Settings"][nodeKey][key] == null)
					doc["Settings"][nodeKey].AppendChild(doc.CreateElement(key));

				doc["Settings"][nodeKey][key].InnerText = value;
				doc.Save(filePathName);
			}
			catch (XmlException ex)
			{
				logger.Error(ex, $"Cannot save <Settings><{nodeKey}><{key}> to {filePathName}: {ex.Message}");
			}
		}

		private string Default(string key)
		{
			switch (key)
			{
				case "ChannelName":
					return "RiskManager";
				case "AggregatedQuotesEnabled":
					return bool.TrueString;
				case "Host":
					return "54.166.100.68";
				case "VirtualHost":
					return "/c2_internal";
				case "Port":
					return "5672";
				case "HeartbeatSeconds":
					return "60";
				case "User":
					return "mk_mv_bs";
				case "Password":
					return "0UsR-5pw8aQi@6";
				case "QueuePrefix":
					return "trade1";
				case "DataCommandExchange":
					return "datacmds";
				case "DataCommandExchangeType":
					return "topic";
				case "DataCommandGetQuoteRoutingKey":
					return "qcmd";
				case "QuotesStreamExchange":
					return "quotes_stream";
				case "QuotesStreamExchangeType":
					return "topic";
				case "QuotesStreamBindingKey":
					return "quote";
				case "StatsExchange":
					return "iqfeed_control_bus";
				case "StatsExchangeType":
					return "topic";
				case "StatsBindingKey":
					return "#";
				case "SendConnectionAlarmEmails":
					return bool.FalseString;
				default:
					logger.Error("Unknown RabbitQuoteClient Setting requested: " + key);
					return "0";
			}
		}
	}
}
