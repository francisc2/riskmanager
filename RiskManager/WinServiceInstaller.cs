﻿using System.ComponentModel;
using System.Configuration.Install;

namespace RiskManager
{
    [RunInstaller(true)]
    public partial class WinServiceInstaller : Installer
    {
        public WinServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
