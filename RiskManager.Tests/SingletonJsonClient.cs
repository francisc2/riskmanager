﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace RiskManager.Tests
{
    public class SingletonJsonClient
    {
        private static readonly IServiceClient instance = new JsonServiceClient(IntegrationTest.BaseUri);

        public static IServiceClient Instance
        {
            get
            {
                return instance;
            }
        }

        private SingletonJsonClient()
        {
        }
    }

}
