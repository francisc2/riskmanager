﻿using Funq;
using System;
using ServiceStack;
using NUnit.Framework;
using System.Threading.Tasks;
using System.Collections.Generic;
using RiskManager.ServiceModel;
using C2Enums;

namespace RiskManager.Tests
{
    public class IntegrationTest
    {
        public const string BaseUri = "http://localhost:8095/";
        private readonly ServiceStackHost appHost;

        class AppHost : AppHostHttpListenerPoolBase
        {
            public AppHost() : base(nameof(IntegrationTest), typeof(ServiceInterface.APIServices).Assembly) { }

            public override void Configure(Container container)
            {
            }
        }

        public IntegrationTest()
        {
            appHost = new AppHost()
                .Init()
                .Start(BaseUri);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        public IServiceClient CreateClient() => new JsonServiceClient(BaseUri);

        [Test]
        public void Can_call_Hello_Service()
        {
            var client = CreateClient();

            var response = client.Get(new Hello { Name = "World" });

            Assert.That(response.Result, Is.EqualTo("Hello, World!"));
        }

        [Test]
        public void Can_call_TakeAvailableSize()
        {
            var client = CreateClient();

            var response = client.Post(new TakeAvailableSize("@ESH21", 49, "1"));
            Assert.That(response.RiskResultCode, Is.EqualTo(RiskResultCode.Approved));
        }

        [Test]
        public void MultiThreaded_TakeAvailableSize()
        {
            var client = RiskManager.Tests.SingletonJsonClient.Instance;
            client.Post(new TakeAvailableSize("@ESH21", 49, "1"));
            System.Threading.Thread.SpinWait(100000);

            List<Task> tests = new List<Task>();
            for (int i = 0; i < 5; i++)
            {
                tests.Add(Task.Run(SimpleTakeAvailableSize));
            }
            Task.WaitAll(tests.ToArray());
            foreach(var test in tests)
                Assert.That(test.GetResult(), Is.AnyOf(RiskResultCode.Approved, RiskResultCode.NoSize, RiskResultCode.PartialApproval));
        }


        private RiskResultCode SimpleTakeAvailableSize()
        {
            var client = RiskManager.Tests.SingletonJsonClient.Instance;
            var response = client.Post(new TakeAvailableSize("@ESH21", 49, "1"));
            return response.RiskResultCode;
        }
    }
}