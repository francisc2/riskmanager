﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack;

namespace RiskManager.ServiceModel
{
    [ApiResponse]
    public class BaseResponse: IHasResponseStatus
    {
		static string OK = System.Net.HttpStatusCode.OK.ToString();


		/// <summary>
		/// Default Status is OK
		/// </summary>
		public BaseResponse()
        {
            ResponseStatus = new ResponseStatus(OK, string.Empty);
        }

		public BaseResponse(System.Net.HttpStatusCode statusCode, string errorDescription)
        {
            ResponseStatus = new ResponseStatus(statusCode.ToString(), errorDescription);
        }


		[ApiMember(Description = @"Status of request",  IsRequired = true)]
		public ResponseStatus ResponseStatus { get; set; }
    }
}
