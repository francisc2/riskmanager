﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace RiskManager.ServiceModel
{
    [Api]
    [Route("/TakeAvailableSize", "POST", Summary = @"Request to place an order for OrderQty @ FullC2Symbol")]
    [Restrict(RequestAttributes.InSecure | RequestAttributes.InternalNetworkAccess, LocalhostOnly = true, VisibleInternalOnly = true)]
    public class TakeAvailableSize: IReturn<TakeAvailableSizeResponse>
    {
        public TakeAvailableSize(string fullC2Symbol, int orderQty, string side)
        {
            this.FullC2Symbol = fullC2Symbol;
            this.OrderQty = orderQty;
            this.Side = side;
        }

        [ApiMember(Description = @"FullC2Symbol", IsRequired = true)]
        public string FullC2Symbol { get; set; }

        [ApiMember(Description = @"OrderQty", IsRequired = true)]
        public int OrderQty { get; set; }

        //[ApiMember(Description = @"Account", IsRequired = true)]
        //public string Account { get; set; }

        //[ApiMember(Description = @"BrokerId", IsRequired = true)]
        //public BrokerId BrokerId { get; set; }

        [ApiMember(Description = @"Side", IsRequired = true)]
        public string Side { get; set; }

        //[ApiMember(Description = @"OrigClOrdId", IsRequired = true)]
        //public string OrigClOrdId { get; set; }

        //[ApiMember(Description = @"SignalId", IsRequired = false)]
        //public int SignalId { get; set; }

        //[ApiMember(Description = @"SystemId", IsRequired = false)]
        //public int SystemId { get; set; }
        
    }

    [ApiResponse]
    public class TakeAvailableSizeResponse : BaseResponse
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorCode">The C2Enums.RiskResultCode</param>
        /// <param name="approvedQty">Approved Quantity to trade</param>
        /// <param name="sizeAvailable">Market Size Available to trade before this risk check</param>
        /// <param name="sizeRemaining">Market Size Remaining to trade after this risk check</param>
        /// <param name="logMsg">Message to log</param>
        /// <param name="marketPrice">The current marketData price, if available</param>
        public TakeAvailableSizeResponse(C2Enums.RiskResultCode errorCode, long approvedQty, long sizeAvailable, long sizeRemaining, string logMsg, decimal marketPrice)
        {
            this.ApprovedQty = approvedQty;
            this.RiskResultCode = errorCode;
            this.SizeAvailable = sizeAvailable;
            this.SizeRemaining = sizeRemaining;
            this.Message = logMsg;
            this.MarketPrice = marketPrice;
        }

        [ApiMember(Description = @"RiskResultCode", IsRequired = true)]
        public C2Enums.RiskResultCode RiskResultCode { get; private set; }


        /// <summary>
        /// Approved Quantity to trade
        /// </summary>
        [ApiMember(Description = @"Approved Quantity to trade", IsRequired = true, DataType = "long")]
        public long ApprovedQty { get; private set; }


        /// <summary>
        /// Market Size Available to trade before this risk check
        /// </summary>
        [ApiMember(Description = @"Market Size Available to trade before this risk check", IsRequired = true, DataType = "long")]
        public long SizeAvailable { get; private set; }


        /// <summary>
        /// Market Size Remaining to trade after this risk check
        /// </summary>
        [ApiMember(Description = @"Market Size Remaining to trade after this risk check", IsRequired = true, DataType = "long")]
        public long SizeRemaining { get; private set; }

        /// <summary>
        /// Message to log
        /// </summary>
        [ApiMember(Description = @"Message to log", IsRequired = true, DataType = "string")]
        public string Message { get; private set; }


        /// <summary>
        /// The current marketData price, if available
        /// </summary>
        [ApiMember(Description = @"The current marketData price, if available", IsRequired = true, DataType = "decimal")]
        public decimal MarketPrice { get; private set; }
    }
}
