﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace RiskManager.ServiceModel
{
    [Api]
    [Route("/PrefetchDataStreams", "GET", Summary = @"Start data streams for popular instruments")]
    [Restrict(RequestAttributes.InSecure | RequestAttributes.InternalNetworkAccess, LocalhostOnly = true, VisibleInternalOnly = true)]

    public class PrefetchDataStreams : IReturn<PrefetchDataStreamsResponse>
    {
        public PrefetchDataStreams()
        {
        }
    }

    [ApiResponse]
    public class PrefetchDataStreamsResponse : BaseResponse
    {
        public string Result { get; set; }
    }
}
